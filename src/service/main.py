import os
from time import time

import mlflow
import pandas as pd
import uvicorn
from fastapi import FastAPI
from prometheus_client import Counter, Gauge, Histogram
from pydantic import BaseModel, Field

mlflow.set_tracking_uri(os.environ["TRACKING_URI"])
MODEL = mlflow.pyfunc.load_model(os.environ["MODEL_URI"])
APP = FastAPI()

## Define Prometheus metrics
INFERENCE_COUNT = Counter("inference_count", "The number of inference requests")
INFERENCE_LATENCY = Histogram("inference_latency", "Latency of the inference requests")
INFERENCE_DIST = Gauge("prediction_distribution", "Distribution of model inference")


class PredictionItem(BaseModel):

    sentence_1: str = Field(..., description="Sentence 1")
    sentence_2: str = Field(..., description="Sentence 2")


@APP.post("/predict")
async def predict(item: PredictionItem) -> float:
    INFERENCE_COUNT.inc()
    start_time = time()

    df = pd.DataFrame([{"sentence_1": item.sentence_1, "sentence_2": item.sentence_2}])
    prediction = MODEL.predict(df)

    INFERENCE_LATENCY.observe(time() - start_time)
    INFERENCE_DIST.set(prediction)

    return prediction


def main():
    uvicorn.run(APP, host="0.0.0.0", port=int(os.environ["PORT"]))

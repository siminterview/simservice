FROM python:3.11-slim

ENV PORT=8000

WORKDIR /opt

RUN pip install poetry

COPY ./*.toml /opt/
RUN poetry config virtualenvs.create false && poetry install --no-interaction --no-ansi

COPY ./src /opt/src
CMD ["python", "src/service/main.py"]

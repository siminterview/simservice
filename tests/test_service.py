import os

from fastapi.testclient import TestClient

from src.service.main import APP

client = TestClient(APP)


def test_predict():
    response = client.post("/predict",
        json={
            "sentence_1": "Sentence 1",
            "sentence_2": "Sentence 2"})
    assert response.status_code == 200
    assert response.json() > .9
